import io.toro.twitter.connector.TwitterConnector
import io.toro.twitter.exception.TwitterExceptionClass
import spock.lang.Ignore
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise
import spock.lang.Unroll
import twitter4j.GeoLocation
import twitter4j.GeoQuery
import twitter4j.OEmbedRequest
import twitter4j.Paging
import twitter4j.TwitterException

@Unroll
@Stepwise
class TwitterTest extends Specification {

//	static {
//		LicenseManagerProperties.setProductKeyProvider( new ProductKeyProvider( 'esb' ) );
//		LicenseManagerProperties.setLicensePasswordProvider( new ToroPublicKeyPasswordProvider(
//				'****COMMODORE64BASICV2****64KRAMSYSTEM38911BASICBYTESFREEREADY.' ) )
//	}

	/** Timeline Resources **/

	def 'getMentions'(){
		setup:
		def response = TwitterConnector.twitterGetMentions( 'test' )
		expect:
		response != null
	}

	def 'bad-getMentions'(){
		when:
		TwitterConnector.twitterGetMentions( 'x' )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'getUserTimeline'(){
		setup:
		def response = TwitterConnector.twitterGetUserTimeline( 'test' )
		response.each {
			if( !response.isFavorited ) {
				if ( response.text[ ctr ].contains ( 'command:' ) ){
					println 'Command Acknowledged!'
					'test'.twitterCreateFavorite(response.id)
					'test'.twitterRetweetStatus(response.id)
				}
				if ( response.text[ ctr ].contains( 'log:' )){
					println 'Log Acknowledged!'
					'test'.twitterCreateFavorite(response.id)
					'test'.twitterRetweetStatus(response.id)
				}
				if ( response.text[ ctr ].contains('esb: shutdown' ) ){
					println 'Acknowledged, Bye!'
					'test'.twitterCreateFavorite(response.id)
					'test'.twitterRetweetStatus(response.id)
				}
			}
		}
//		expect:
//		response[0] != null
	}

	def 'bad-getUserTimeline'(){
		when:
		TwitterConnector.twitterGetUserTimeline( 'x' )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'getHomeTimeline'(){
		setup:
		def response = TwitterConnector.twitterGetHomeTimeline( 'test' )
		expect:
		response[0] != null
	}

	def 'bad-getHomeTimeline'(){
		when:
		TwitterConnector.twitterGetHomeTimeline( 'x' )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'getRetweetOfMe'(){
		setup:
		def response = TwitterConnector.twitterGetRetweetOfMe( 'test' )
		expect:
		response[0] == null
	}

	def 'bad-getRetweetOfMe'(){
		when:
		TwitterConnector.twitterGetRetweetOfMe( 'x' )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	/** Tweets Resources **/

	def 'getRetweets'(){
		setup:
		def response = TwitterConnector.twitterGetRetweet( 'test', 631691746997612545 )
		expect:
		response[0].retweeted != null
	}

	def 'bad-getRetweet'(){
		when:
		TwitterConnector.twitterGetRetweet( 'test', 0 )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	def 'getRetweeterIds'() {
		setup:
		def response = TwitterConnector.twitterGetRetweeterIds( 'test', 631691746997612545, -1 )
		expect:
		response.ids[0] != null
	}

	def 'bad-getRetweeterIds'(){
		when:
		TwitterConnector.twitterGetRetweeterIds( 'test', 6132358863, -1 )
		then:
		final TwitterException exception = thrown()
		exception.message == '''403:The request is understood, but it has been refused. An accompanying error message will explain why. This code is used when requests are being denied due to update limits (https://support.twitter.com/articles/15364-about-twitter-limits-update-api-dm-and-following).
message - Sorry, you are not authorized to see this status.
code - 179
'''
	}

	def 'showStatus'() {
		setup:
		def response = TwitterConnector.twitterShowStatus( 'test', 596479767861297152 )
		expect:
		response.id == 596479767861297152
	}

	def 'bad-showStatus'(){
		when:
		TwitterConnector.twitterShowStatus( 'test', 6132358863 )
		then:
		final TwitterException exception = thrown()
		exception.message == '''403:The request is understood, but it has been refused. An accompanying error message will explain why. This code is used when requests are being denied due to update limits (https://support.twitter.com/articles/15364-about-twitter-limits-update-api-dm-and-following).
message - Sorry, you are not authorized to see this status.
code - 179
'''
	}

	@Shared createdStatus
	def 'updatestatus'(){
		setup:
		createdStatus = TwitterConnector.twitterUpdateStatus( 'test', 'this is a task ' + new Random().nextInt(1000) )
		expect:
		createdStatus.text.contains( 'this is a task' ) == true
	}

	def 'bad-updatestatus'(){
		when:
		TwitterConnector.twitterUpdateStatus( 'x', 'test' )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'destroyStatus'() {
		setup:
		def response = TwitterConnector.twitterDestroyStatus( 'test', createdStatus.id )
		expect:
		response.id == createdStatus.id
	}

	def 'bad-destroyStatus'(){
		when:
		TwitterConnector.twitterDestroyStatus( 'test', 298556668 )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	def 'bad-retweetStatus'(){
		when:
		TwitterConnector.twitterRetweetStatus( 'test', 298556668 )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	def 'getOEmbed'() {
		setup:
		def response = TwitterConnector.twitterGetOEmbed( 'test', new OEmbedRequest( 240192632003911681, 'http://samuraism.com/' ) )
		expect:
		response != null
	}

	def 'bad-getOEmbed'() {
		when:
		TwitterConnector.twitterGetOEmbed( 'test', new OEmbedRequest( 111111111111111111L, 'http://samuraism.com/ ' ) )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	def 'updateStatusWithMedia'() {
		setup:
		def response = TwitterConnector.twitterUpdateStatusWithMedia( 'test', 'xxx', new File( '../src/mustache.jpg' ) )
		expect:
		response.text.contains( 'xxx' )
	}

	def 'bad-updateStatusWithMedia'(){
		when:
		TwitterConnector.twitterUpdateStatusWithMedia( 'x', 'test', null )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'uploadMedia'() {
		setup:
		def response = TwitterConnector.twitterUploadMedia( 'test', new File( '../src/mustache.jpg' ) )
		expect:
		response != null
	}

	def 'bad-uploadMedia'(){
		when:
		TwitterConnector.twitterUploadMedia( 'x', null )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	/** Search Resources  **/

	def 'searchQuery'() {
		setup:
		def response = TwitterConnector.twitterSearch( 'test', 'source:twitter4j yusukey' )
		expect:
		response.maxId !=null
	}

	def 'bad-searchQuery'(){
		when:
		TwitterConnector.twitterUploadMedia( 'x', null )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	/**  Friends & Followers Resources **/

	def 'getFriendsIDs'() {
		setup:
		def response = TwitterConnector.twitterGetFriendsIDs( 'test', -1 )
		expect:
		response.ids[0] != null
	}

	def 'bad-getFriendsIDs'(){
		when:
		TwitterConnector.twitterGetFriendsIDs( 'x', -1 )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'getFollowersIDs'() {
		setup:
		def response = TwitterConnector.twitterGetFollowersIDs( 'test', -1 )
		expect:
		response != null
	}

	def 'bad-getFollowersIDs'(){
		when:
		TwitterConnector.twitterGetFollowersIDs( 'x', -1 )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'lookupFriendships'() {
		setup:
		def response = TwitterConnector.twitterLookupFriendships( 'test', 'textposts', 'ABSCBNNews' )
		expect:
		response[0].id == 353593353
	}

	def 'bad-lookupFriendships'(){
		when:
		TwitterConnector.twitterLookupFriendships( 'x', null, null )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'getIncomingFriendships'() {
		setup:
		def response = TwitterConnector.twitterGetIncomingFriendships( 'test', -1 )
		expect:
		response != null
	}

	def 'bad-getIncomingFriendships'(){
		when:
		TwitterConnector.twitterGetIncomingFriendships( 'x', 0 )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'getOutgoingFriendship'() {
		setup:
		def response = TwitterConnector.twitterGetOutgoingFriendship( 'test', -1 )
		expect:
		response != null
	}

	def 'bad-getOutgoingFriendship'(){
		when:
		TwitterConnector.twitterGetOutgoingFriendship( 'x', 0 )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'createFriendship'() {
		setup:
		def response = TwitterConnector.twitterCreateFriendship( 'test', 353593353 )
		expect:
		response.location == 'Worldwide'
	}

	def 'bad-createFriendship'(){
		when:
		TwitterConnector.twitterCreateFriendship( 'x', -1 )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'destroyFriendship'() {
		setup:
		def response = TwitterConnector.twitterDestroyFriendship( 'test', 353593353 )
		expect:
		response.location == 'Worldwide'
	}

	def 'bad-destroyFriendship'(){
		when:
		TwitterConnector.twitterDestroyFriendship( 'test', 3535353 )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	def 'upodateFriendship'() {
		when:
		sleep(10000)
		def response = TwitterConnector.twitterUpdateFriendship( 'test', 9268382, true, true)
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 403
	}

	def 'bad-upodateFriendship'(){
		when:
		TwitterConnector.twitterUpdateFriendship( 'test', 9260999, true, true)
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	def 'showFriendship'() {
		setup:
		def response = TwitterConnector.twitterShowFriendship( 'test', 'TestToro', 'textposts' )
		expect:
		response != null
	}

	def 'bad-showFriendship'(){
		when:
		TwitterConnector.twitterShowFriendship( 'test', 'TestToro', '-1null' )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	def 'getFriendsList'() {
		setup:
		def response = TwitterConnector.twitterGetFriendsList( 'test', 'textposts', -1 )
		expect:
		response != null
	}

	def 'bad-getFriendsList'(){
		when:
		TwitterConnector.twitterGetFriendsList( 'test', '-1null', -1 )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	def 'getFollowersList'() {
		setup:
		def response = TwitterConnector.twitterGetFollowersList( 'test', 'textposts', -1 )
		expect:
		response != null
	}

	def 'bad-getFollowersList'(){
		when:
		TwitterConnector.twitterGetFollowersList( 'test', '-1null', -1 )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	/**  Users Resources **/

	def 'getAccountSettings'() {
		setup:
		def response = TwitterConnector.twitterGetAccountSettings( 'test' )
		expect:
		response.screenName == 'johnalthough'
	}

	def 'bad-getAccountSettings'(){
		when:
		TwitterConnector.twitterGetAccountSettings( 'x' )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'verifyCredentials'() {
		setup:
		def response = TwitterConnector.twitterVerifyCredentials( 'test' )
		expect:
		response.name == 'TestToro'
	}

	def 'bad-verifyCredentials'(){
		when:
		TwitterConnector.twitterVerifyCredentials( 'x' )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'updateProfile'() {
		setup:
		def response = TwitterConnector.twitterUpdateProfile( 'test', 'TestToro', 'www.toro.io', 'Philippines', 'Turnkey Online Retail Operations' )
		expect:
		response.location == 'Philippines'
	}

	def 'bad-updateProfile'(){
		when:
		TwitterConnector.twitterUpdateProfile( 'test', 'TestToro', 'toro.io', 'Philippines', 'Turnkey Online Retail Operations' )
		then:
		final TwitterException exception = thrown()
		exception.message == '''403:The request is understood, but it has been refused. An accompanying error message will explain why. This code is used when requests are being denied due to update limits (https://support.twitter.com/articles/15364-about-twitter-limits-update-api-dm-and-following).
message - Account update failed: Url is not valid.
code - 120
'''
	}

	def 'updateProfileBackgroundImage'() {
		setup:
		def response = TwitterConnector.twitterUpdateProfileBackgroundImage( 'test', new File( '../src/toro-2.png' ), true )
		expect:
		response.location == 'Philippines'
	}

	def 'bad-updateProfileBackgroundImage'(){
		when:
		TwitterConnector.twitterUpdateProfileBackgroundImage( 'test', new File( '../src/toro-2.png ' ), true )
		then:
		final TwitterException exception = thrown()
		exception.message == '../src/toro-2.png  is not found.'
	}

	def 'updateProfileImage'() {
		setup:
		def response = TwitterConnector.twitterUpdateProfileImage( 'test', new File( '../src/toro.png' ) )
		expect:
		response.location == 'Philippines'
	}

	def 'bad-updateProfileImage'(){
		when:
		TwitterConnector.twitterUpdateProfileImage( 'test', new File( '../src/toro.png ' ) )
		then:
		final TwitterException exception = thrown()
		exception.message == '../src/toro.png  is not found.'
	}

	def 'getBlocksList'() {
		setup:
		def response = TwitterConnector.twitterGetBlocksList( 'test' )
		expect:
		response != null
	}

	def 'bad-getBlocksList'(){
		when:
		TwitterConnector.twitterGetBlocksList( 'x' )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'getBlocksIDs'() {
		setup:
		def response = TwitterConnector.twitterGetBlocksIDs( 'test' )
		expect:
		response != null
	}

	def 'bad-getBlocksIDs'(){
		when:
		TwitterConnector.twitterGetBlocksIDs( 'x' )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'lookupUsers'() {
		setup:
		def response = TwitterConnector.twitterLookupUsers( 'test', 'ToroTest','textposts' )
		expect:
		response[0].id == 2816305465
	}

	def 'bad-lookupUsers'(){
		when:
		TwitterConnector.twitterLookupUsers( 'x', 'xxx', 'xxxx' )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'showUser'() {
		setup:
		def response = TwitterConnector.twitterShowUser( 'test', 'textposts' )
		expect:
		response.id == 353593353
	}

	def 'bad-showUser'(){
		when:
		TwitterConnector.twitterShowUser( 'test', '-1null' )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	def 'searchUser'() {
		when:
		def response = TwitterConnector.twitterSearchUsers( 'test', 'daren klamer', 10 )
		then:
		noExceptionThrown()
	}

	def 'bad-searchUser'(){
		when:
		TwitterConnector.twitterSearchUsers( 'x', 'xxx', 10 )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'bad-getContributees'() {
		when:
		TwitterConnector.twitterGetContributees( 'test', 'ToroTest')
		then:
		final TwitterException exception = thrown()
		exception.message == '''403:The request is understood, but it has been refused. An accompanying error message will explain why. This code is used when requests are being denied due to update limits (https://support.twitter.com/articles/15364-about-twitter-limits-update-api-dm-and-following).
message - Your credentials do not allow access to this resource
code - 220
'''
	}

	def 'bad-getContributors'() {
		when:
		TwitterConnector.twitterGetContributors( 'test', 'ToroTest')
		then:
		final TwitterException exception = thrown()
		exception.message == '''403:The request is understood, but it has been refused. An accompanying error message will explain why. This code is used when requests are being denied due to update limits (https://support.twitter.com/articles/15364-about-twitter-limits-update-api-dm-and-following).
message - Your credentials do not allow access to this resource
code - 220
'''
	}

	def 'updateProfileBanner'() {
		setup:
		def response = TwitterConnector.twitterUpdateProfileBanner( 'test', new File( '../src/toro-2.png' ) )
		expect:
		response == null
	}

	def 'bad-updateProfileBanner'(){
		when:
		TwitterConnector.twitterUpdateProfileBanner( 'test', new File( '../src/toro-2.png ' ) )
		then:
		final TwitterException exception = thrown()
		exception.message == '../src/toro-2.png  is not found.'
	}

	def 'removeProfileBanner'(){
		setup:
		def response = TwitterConnector.twitterRemoveProfileBanner( 'test' )
		expect:
		response == null
	}

	def 'bad-removeProfileBanner'() {
		when:
		TwitterConnector.twitterRemoveProfileBanner( 'x' )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	@Shared muteUser
	def 'createMute'() {
		setup:
		muteUser = TwitterConnector.twitterCreateMute( 'test', 'PatamaPost' )
		expect:
		muteUser.id == 505402473
	}

	def 'bad-createMute'(){
		when:
		TwitterConnector.twitterCreateMute( 'test', '-1null' )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	def 'getMutesIDs'() {
		setup:
		def response = TwitterConnector.twitterGetMutesIDs( 'test', -1 )
		expect:
		response.ids[0] == muteUser.id
	}

	def 'bad-getMutesIDs'(){
		when:
		TwitterConnector.twitterGetMutesIDs( 'x', -1 )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'getMutesList'() {
		setup:
		def response = TwitterConnector.twitterGetMutesList( 'test', -1 )
		expect:
		response != null
	}

	def 'bad-getMutesList'(){
		when:
		TwitterConnector.twitterGetMutesList( 'x', -1 )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'destroyMute'() {
		setup:
		def response = TwitterConnector.twitterDestroyMute( 'test', 'PatamaPost' )
		expect:
		muteUser.id == 505402473
	}

	def 'bad-destroyMute'(){
		when:
		TwitterConnector.twitterDestroyMute( 'test', '-1null' )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	/** Suggested Users Resources **/

	def 'bad-getUserSuggestions'(){
		when:
		TwitterConnector.twitterGetUserSuggestions( 'x', 'Pulitika' )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'getSuggestedUserCategories'() {
		setup:
		def response = TwitterConnector.twitterGetSuggestedUserCategories( 'test' )
		expect:
		response[0].name != null
	}

	def 'bad-getSuggestedUserCategories'(){
		when:
		TwitterConnector.twitterGetSuggestedUserCategories( 'x' )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'bad-getMemberSuggestions'(){
		when:
		TwitterConnector.twitterGetMemberSuggestions( 'test', 'twitter' )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	/** Favorites Resources **/

	@Shared createFave
	def 'createFavorite'(){
		setup:
		createFave = TwitterConnector.twitterCreateFavorite( 'test', 631596488095764480 )
		expect:
		createFave.id == 631596488095764480
	}

	def 'bad-createFavorite'(){
		when:
		TwitterConnector.twitterCreateFavorite( 'test', 6659515445455545 )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	def 'getFavorites'() {
		setup:
		def response = TwitterConnector.twitterGetFavorites( 'test' )
		expect:
		response[0].id != null
	}

	def 'bad-getFavorites'(){
		when:
		TwitterConnector.twitterGetFavorites( 'x' )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'destroyFavorite'() {
		setup:
		def response = TwitterConnector.twitterDestroyFavorite( 'test', createFave.id )
		expect:
		response.id == createFave.id
	}

	def 'bad-destroyFavorite'(){
		when:
		TwitterConnector.twitterDestroyFavorite( 'test', 6659515445455545 )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	/** List Resources **/

	def 'getUserLists'() {
		setup:
		def response = TwitterConnector.twitterGetUserLists( 'test', 'TestToro' )
		expect:
		response[0] != 0
	}

	def 'bad-getUserLists'(){
		when:
		TwitterConnector.twitterGetUserLists( 'test', '-1null' )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	def 'getUserListStatuses'() {
		setup:
		def response = TwitterConnector.twitterGetUserListStatuses( 'test', 4343, new Paging(1) )
		expect:
		response != null
	}

	def 'bad-getUserListStatuses'(){
		when:
		TwitterConnector.twitterGetUserListStatuses( 'test', 2222, new Paging(1) )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	def 'bad-destroyUserListMember'(){
		when:
		TwitterConnector.twitterDestroyUserListMember( 'test', 353593353, 'textposts' )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	def 'getUserListMemberships'() {
		setup:
		def response = TwitterConnector.twitterGetUserListMemberships( 'test', 'ToroTest', -1 )
		expect:
		response != null
	}

	def 'bad-getUserListMemberships'(){
		when:
		TwitterConnector.twitterGetUserListMemberships( 'test', '-1null', -1 )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	def 'getUserListSubscribers'() {
		setup:
		def response = TwitterConnector.twitterGetUserListSubscribers( 'test', 4343, -1 )
		expect:
		response[0] != null
	}

	def 'bad-getUserListSubscribers'(){
		when:
		TwitterConnector.twitterGetUserListSubscribers( 'test', 4341633, -1 )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	def 'createUserListSubscription'() {
		setup:
		def response = TwitterConnector.twitterCreateUserListSubscription( 'test', 14537765 )
		expect:
		response.id == 14537765
	}

	def 'bad-createUserListSubscription'(){
		when:
		TwitterConnector.twitterCreateUserListSubscription( 'test', 4341633 )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	def 'showUserListSubscription'() {
		setup:
		def response = TwitterConnector.twitterShowUserListSubscription( 'test', 4343, 3142269147 )
		expect:
		response.location == 'Philippines'
	}

	def 'bad-showUserListSubscription'(){
		when:
		TwitterConnector.twitterShowUserListSubscription( 'test', 4341633, 3142269147 )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	def 'destroyUserListSubscription'() {
		setup:
		def response = TwitterConnector.twitterDestroyUserListSubscription( 'test', 14537765 )
		expect:
		response.id == 14537765
	}

	def 'bad-destroyUserListSubscription'(){
		when:
		TwitterConnector.twitterDestroyUserListSubscription( 'test', 14537765 )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	def 'createUserListMembers'() {
		setup:
		def response = TwitterConnector.twitterCreateUserListMembers( 'test', 14537765 ,'TestToro', 'textposts' )
		expect:
		response.id == 14537765
	}

	def 'bad-createUserListMembers'(){
		when:
		TwitterConnector.twitterCreateUserListMembers( 'test', 145377, 'TestToro' )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	def 'showUserListMembership'() {
		setup:
		def response = TwitterConnector.twitterShowUserListMembership( 'test', 14537765, 15872418 )
		expect:
		response.location == 'Manila, Philippines'
	}

	def 'bad-showUserListMembership'(){
		when:
		TwitterConnector.twitterShowUserListMembership( 'test', 14537765, 158724 )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	def 'twitterGetUserListMembers'() {
		setup:
		def response = TwitterConnector.twitterGetUserListMembers( 'test', 14537765, -1 )
		expect:
		response.name[0] == 'ABSCBN RealiTV'
	}

	def 'bad-twitterGetUserListMembers'(){
		when:
		TwitterConnector.twitterGetUserListMembers( 'test', 145375, -1 )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	@Shared createdList
	def 'createUserList'(){
		setup:
		createdList = TwitterConnector.twitterCreateUserList( 'test', 'try', true, 'try this method' )
		expect:
		createdList.name == 'try'
	}

	def 'bad-createUserList'(){
		when:
		TwitterConnector.twitterCreateUserList( 'x', '', true, '' )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'bad-updateUserList'(){
		when:
		TwitterConnector.twitterUpdateUserList( 'test', 2171162, '', true, '' )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}
	
	@Ignore
	def 'destroyUserList'() {
		setup:
		def response = TwitterConnector.twitterDestroyUserList( 'test', createdList.id )
		expect:
		response.id == createdList.id
	}
	
	def 'bad-destroyUserList'(){
		when:
		TwitterConnector.twitterDestroyUserList( 'test', 217116281 )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	def 'showUserList'() {
		setup:
		def response = TwitterConnector.twitterShowUserList( 'test', 4343 )
		expect:
		response.uri == '/kitson/lists/thought-leaders'
	}

	def 'bad-showUserList'(){
		when:
		TwitterConnector.twitterShowUserList( 'test', 217116281 )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	def 'getUserListSubscriptions'() {
		setup:
		def response = TwitterConnector.twitterGetUserListSubscriptions( 'test', 'ToroTest', -1 )
		expect:
		response != null
	}

	def 'bad-getUserListSubscriptions'(){
		when:
		TwitterConnector.twitterGetUserListSubscriptions( 'test', '-1null', -1 )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	/** Places and Geo Resources  **/

	def 'getGeoDetails'() {
		setup:
		def response = TwitterConnector.twitteretGeoDetails( 'test', 'df51dec6f4ee2b2c' )
		expect:
		response.country == 'United States'
	}

	def 'bad-getGeoDetails'(){
		when:
		TwitterConnector.twitteretGeoDetails( 'test', '-1null' )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	def 'reverseGeoCode'() {
		setup:
		def response = TwitterConnector.twitterReverseGeoCode( 'test', new GeoQuery( new GeoLocation( 13.0, 122.0 ) ) )
		expect:
		response[0].name != null
	}

	def 'bad-reverseGeoCode'(){
		when:
		TwitterConnector.twitterReverseGeoCode( 'x', new GeoQuery( new GeoLocation( 0, 0 ) ) )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'searchPlaces'() {
		setup:
		def response = TwitterConnector.twitterSearchPlaces( 'test', new GeoQuery( new GeoLocation( 13.0, 122.0 ) ) )
		expect:
		response[0].country == 'Republika ng Pilipinas'
	}

	def 'bad-searchPlaces'(){
		when:
		TwitterConnector.twitterSearchPlaces( 'test', new GeoQuery( '192.168.1.1' ) )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	def 'getSimilarPlaces'() {
		setup:
		def response = TwitterConnector.twitterGetSimilarPlaces( 'test', new GeoLocation(13,122), 'Twitter HQ', '247f43d441defc03', '795 Folsom St' )
		expect:
		response != null
	}

	def 'bad-getSimilarPlaces'(){
		when:
		TwitterConnector.twitterGetSimilarPlaces( 'x', new GeoLocation( 0, 0 ), null, null, null )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	/** Trends Resources **/

	def 'getPlaceTrends'() {
		setup:
		def response = TwitterConnector.twitterGetPlaceTrends( 'test', 1 )
		expect:
		response.location.name == 'Worldwide'
	}

	def 'bad-getPlaceTrends'(){
		when:
		TwitterConnector.twitterGetPlaceTrends( 'test', 2 )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	def 'getAvailableTrends'() {
		setup:
		def response = TwitterConnector.twittergGetAvailableTrends( 'test' )
		expect:
		response.name[0] != null
	}

	def 'bad-getAvailableTrends'(){
		when:
		TwitterConnector.twittergGetAvailableTrends( 'x' )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'getClosestTrends'() {
		setup:
		def response = TwitterConnector.twitterGetClosestTrends( 'test', new GeoLocation(13,122) )
		expect:
		response != null
	}

	def 'bad-getClosestTrends'(){
		when:
		TwitterConnector.twitterGetClosestTrends( 'x', null )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	/** Spam Reporting Resource **/

	def 'reportSpam'() {
		setup:
		def response = TwitterConnector.twitterReportSpam( 'test', 'ToroTest' )
		expect:
		response.id == 2816305465
	}

	def 'bad-reportSpam'(){
		when:
		TwitterConnector.twitterReportSpam( 'test', '-1null' )
		then:
		final TwitterException exception = thrown()
		exception.statusCode == 404
	}

	/** Help Resources **/

	def 'getAPIConfiguration'() {
		setup:
		def response = TwitterConnector.twitterGetAPIConfiguration( 'test' )
		expect:
		response != null
	}

	def 'bad-getAPIConfiguration'(){
		when:
		TwitterConnector.twitterGetAPIConfiguration( 'x' )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'getLanguages'() {
		setup:
		def response = TwitterConnector.twitterGetLanguages( 'test' )
		expect:
		response[1].name == 'English'
	}

	def 'bad-getLanguages'(){
		when:
		TwitterConnector.twitterGetLanguages( 'x' )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'getPrivacyPolicy'() {
		setup:
		def response = TwitterConnector.twitterGetPrivacyPolicy( 'test' )
		expect:
		response.contains('Twitter Privacy Policy')
	}

	def 'bad-getPrivacyPolicy'(){
		when:
		TwitterConnector.twitterGetPrivacyPolicy( 'x' )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'getTermsOfService'() {
		setup:
		def response = TwitterConnector.twitterGetTermsOfService( 'test' )
		expect:
		response.contains('Twitter Terms of Service')
	}

	def 'bad-getTermsOfService'(){
		when:
		TwitterConnector.twitterGetTermsOfService( 'x' )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}

	def 'getRateLimitStatus'() {
		setup:
		def response = TwitterConnector.twitterGetRateLimitStatus( 'test', "trends","application","users","saved_searches","geo","direct_messages","blocks","favorites","statuses","followers","help","friends","search","friendships","account","lists" )
		expect:
		response != null
	}

	def 'bad-getRateLimitStatus'(){
		when:
		TwitterConnector.twitterGetRateLimitStatus( 'x', null )
		then:
		final TwitterExceptionClass exception = thrown()
		exception.message == 'No credentials on alias'
	}
}
