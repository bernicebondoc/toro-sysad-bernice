package io.toro.twitter.exception

/**
 * exception handler
 * @author ericjamesdaluz
 */
class TwitterExceptionClass extends Exception {

    TwitterExceptionClass ( String message ) {
        super( message )
    }

    TwitterExceptionClass ( Throwable t ) {
        super( t )
    }
}
