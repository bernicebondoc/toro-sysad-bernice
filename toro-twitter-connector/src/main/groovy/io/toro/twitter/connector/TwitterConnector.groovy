package io.toro.twitter.connector

import groovy.transform.CompileStatic
import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode
import io.toro.twitter.exception.TwitterExceptionClass
import twitter4j.AccountSettings
import twitter4j.Category
import twitter4j.DirectMessage
import twitter4j.Friendship
import twitter4j.GeoLocation
import twitter4j.GeoQuery
import twitter4j.IDs
import twitter4j.Location
import twitter4j.OEmbedRequest
import twitter4j.PagableResponseList
import twitter4j.Paging
import twitter4j.Place
import twitter4j.Query
import twitter4j.QueryResult
import twitter4j.RateLimitStatus
import twitter4j.Relationship
import twitter4j.ResponseList
import twitter4j.SavedSearch
import twitter4j.Status
import twitter4j.StatusUpdate
import twitter4j.Trends
import twitter4j.Twitter
import twitter4j.TwitterAPIConfiguration
import twitter4j.TwitterFactory
import twitter4j.UploadedMedia
import twitter4j.User
import twitter4j.UserList
import twitter4j.api.HelpResources.Language
import twitter4j.conf.ConfigurationBuilder

import com.toro.licensing.LicenseManager
import com.toro.licensing.exception.InvalidLicenseException

/**
 * Twitter Connector<br/>
 * <a href="http://twitter4j.org/en/api-support.html">See External Resources Here</a>
 * @author ericdaluz
 *
 */
@CompileStatic
@SuppressWarnings( ['UnnecessaryGetter', 'LineLength'] )
class TwitterConnector {
	static Map apiKeyMap = [ : ]

	private static final boolean AVAILABLE_FOR_FREE = true

	TwitterConnector() {
		if ( !( LicenseManager.getInstance().getLicense().isFree() && AVAILABLE_FOR_FREE ) )
		throw new InvalidLicenseException('This connector is not available for free')
	}

	/**
	 * common code to save credentials- to http://localhost:8080
	 * @param alias name on toroproperties credentials
	 * @param consumerKey is a secret string for the consumer
	 * @param consumerSecret is a secret string for the consumer
	 * @param accessToken the unique sequence characters to define a user
	 * @param accessTokenSecret is a secret string for the client
	 * @return returns success
	 */
	@TypeChecked( TypeCheckingMode.SKIP )
	static String twitterSaveTokenAndSecret( String alias, String consumerKey, String consumerSecret,
			String accessToken, String accessTokenSecret ) {
		"twitter.${alias}".saveTOROProperty( "${consumerKey}||${consumerSecret}||${accessToken}||${accessTokenSecret}" )
		"Successfully saved alias ${alias}."
	}

//	static main ( args ){
//		twitterSaveTokenAndSecret( 'test', 'RPkKrwvvG6KszMTG4ISkETygH', 'P7dKtHUExfP57Kdj7usCB3zMuYwiHxMuCQWEVjkBdl9KmHT5KH', '3142269147-ywdXezozoEZ3Yp9nO0nZ36mkHb4dTnEUCV9Rsr2', 'YEnJz1Rky3IojctMce0qKdAWcofiZInqQrBKtPVhy4uTM' )
//	}

	/*
	 * common code to get saved credentials
	 * @param apiKeyAlias
	 * @return returns an array list of credentials
	 */
	@TypeChecked( TypeCheckingMode.SKIP )
	private static String[] getApiKey( String apiKeyAlias ) {
		String tempKey = apiKeyAlias.getTOROProperty()
		if ( !tempKey ) {
			throw new TwitterExceptionClass( 'No credentials on alias' )
		}
		String[] tokens = tempKey.split( '\\|\\|' )
		apiKeyMap.apiKeyAlias = tokens
		tokens
	}

	/*
	 * Set authorization keys<br/>
	 * <a href="http://twitter4j.org/en/configuration.html">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @return returns ConfigurationBuilder object
	 */
	@TypeChecked( TypeCheckingMode.SKIP )
	private static Object authorizeTwitter( String alias ) {
		String[] keys = getApiKey( "twitter.${alias}" )
		ConfigurationBuilder cb = new ConfigurationBuilder()
		cb.setDebugEnabled(true)
				.setOAuthConsumerKey( keys[ 0 ] )
				.setOAuthConsumerSecret( keys[ 1 ] )
				.setOAuthAccessToken( keys[ 2 ] )
				.setOAuthAccessTokenSecret( keys[ 3 ] )
		cb
	}

	/*
	 * Method that handles all normal calls<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/TweetsResources.html">See Here</a>
	 * @param alias name on toroproperties credentials
	 * @param message the status to be updated
	 * @return returns information about the update/null
	 */
	@TypeChecked( TypeCheckingMode.SKIP )
	private static Twitter initHandler( String alias ) {
		TwitterFactory tf = new TwitterFactory( authorizeTwitter( alias ).build() )
		Twitter twitter = tf.getInstance()
		twitter
	}

	//Timeline Resources

	/**
	 * Get Mentions on your Twitter Account<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/TimelinesResources.html#getMentionsTimeline()">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @return returns list of status with mentions on timeline
	 * @throws TwitterExceptionClass
	 */
	static List<Status> twitterGetMentions( String alias ) throws TwitterExceptionClass {
		initHandler( alias ).getMentionsTimeline()
	}

	/**
	 * Get tweets from the user's timeline<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/TimelinesResources.html#getUserTimeline()">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @return returns list of status with mentions on timeline
	 * @throws TwitterExceptionClass
	 */
	static List<Status> twitterGetUserTimeline( String alias ) throws TwitterExceptionClass {
		initHandler( alias ).getUserTimeline()
	}

	/**
	 * Get tweets from followers and followed users<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/TimelinesResources.html#getHomeTimeline()">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @return returns list of status on timeline
	 * @throws TwitterExceptionClass
	 */
	static List<Status> twitterGetHomeTimeline( String alias ) throws TwitterExceptionClass {
		initHandler( alias ).getHomeTimeline()
	}

	/**
	 * Get user retweeted status of other users<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/TimelinesResources.html#getRetweetsOfMe()">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @return returns list of status on timeline
	 * @throws TwitterExceptionClass
	 */
	static List<Status> twitterGetRetweetOfMe( String alias ) throws TwitterExceptionClass {
		initHandler( alias ).getRetweetsOfMe()
	}

	//Tweet Resources

	/**
	 * Get user retweeted status<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/TimelinesResources.html#getRetweetsOfMe()">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param statusId the identification number of the status
	 * @return returns list of retweets of the specified status
	 * @throws TwitterExceptionClass
	 */
	static List<Status> twitterGetRetweet( String alias, long statusId ) throws TwitterExceptionClass {
		initHandler( alias ).getRetweets( statusId )
	}

	/**
	 * Get the id's of retweets<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/TweetsResources.html#getRetweeterIds(long)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param statusId the identification number of retweeters
	 * @param page the pagination number
	 * @return returns list of retweets of the specified status
	 * @throws TwitterExceptionClass
	 */
	static Object twitterGetRetweeterIds( String alias, long statusId, long page ) throws TwitterExceptionClass {
		initHandler( alias ).getRetweeterIds( statusId, page )
	}

	/**
	 * Get the specified tweet through the use of the tweet id<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/TweetsResources.html#showStatus(long)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param statusId  the identification number of the status
	 * @return returns the status for the id number
	 * @throws TwitterExceptionClass
	 */
	static Status twitterShowStatus( String alias, long statusId ) throws TwitterExceptionClass {
		initHandler( alias ).showStatus(statusId)
	}

	/**
	 * Delete tweet<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/TweetsResources.html#destroyStatus(long)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param statusId  the identification number of the status
	 * @return returns the info about status
	 * @throws TwitterExceptionClass
	 */
	static Status twitterDestroyStatus( String alias, long statusId ) throws TwitterExceptionClass {
		initHandler( alias ).destroyStatus(statusId)
	}

	/**
	 * Post an update or tweet into Twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/TweetsResources.html#updateStatus(java.lang.String)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param message the status to be updated
	 * @return returns information about the status
	 * @throws TwitterExceptionClass
	 */
	static Status twitterUpdateStatus( String alias, String message ) throws TwitterExceptionClass {
		initHandler( alias ).updateStatus( message )
	}

	/**
	 * Retweet a status of another user<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/TweetsResources.html#retweetStatus(long)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param statusId  the identification number of the status
	 * @return returns retweeted status
	 * @throws TwitterExceptionClass
	 */
	static Status twitterRetweetStatus( String alias, long statusId ) throws TwitterExceptionClass {
		initHandler( alias ).retweetStatus(statusId)
	}

	/**
	 * Get twitter O Embed<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/TweetsResources.html#getOEmbed(twitter4j.OEmbedRequest)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param req is the oembedded object
	 * @return returns information on the creation of an oembedded of a Tweet on third party sites
	 * @throws TwitterExceptionClass
	 */
	static Object twitterGetOEmbed( String alias, OEmbedRequest req ) throws TwitterExceptionClass {
		initHandler( alias ).getOEmbed( req )
	}

	/*
	 * Overloaded call handler for file uploads<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/TweetsResources.html">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param message the status to be updated
	 * @return returns information about the update/null
	 */
	@TypeChecked( TypeCheckingMode.SKIP )
	private static StatusUpdate initHandler( String alias, String message, File file ) {
		LicenseManager.getInstance().getLicense()
		TwitterFactory tf = new TwitterFactory( authorizeTwitter( alias ).build() )
		Twitter twitter = tf.getInstance()
		UploadedMedia media = twitter.uploadMedia( file )
		long mId = media.getMediaId()
		StatusUpdate update = new StatusUpdate( message )
		update.setMediaIds( mId )
		update
	}

	/**
	 * Post a tweet or status update with media files<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/TweetsResources.html#updateStatus(java.lang.String)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param message the status to be updated
	 * @param file is the media to be uploaded
	 * @return returns info about uploaded file and status
	 * @throws TwitterExceptionClass
	 */
	static Object twitterUpdateStatusWithMedia( String alias, String message, File file ) throws TwitterExceptionClass {
		initHandler( alias ).updateStatus( initHandler( alias, message, file ) )
	}

	/**
	 * Upload a media on twitter that could be used later<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/TweetsResources.html#uploadMedia(java.io.File)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param file is the media to be uploaded
	 * @return returns information about uploaded file
	 * @throws TwitterExceptionClass
	 */
	static Object twitterUploadMedia( String alias, File file ) throws TwitterExceptionClass {
		initHandler( alias ).uploadMedia( file )
	}

	//Search Resources

	/**
	 * Search with keywords on Twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/SearchResource.html#search(twitter4j.Query)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param query set of filtering strings
	 * @return returns information from the query
	 * @throws TwitterExceptionClass
	 */
	static QueryResult twitterSearch( String alias, String query ) throws TwitterExceptionClass {
		initHandler( alias ).search( new Query( query ) )
	}

	//Direct Message Resources

	/**
	 * Get direct messages on Twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/DirectMessagesResources.html#getDirectMessages()">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @return returns list of direct message
	 * @throws TwitterExceptionClass
	 */
	static List<DirectMessage> twitterGetDirectMessage( String alias ) throws TwitterExceptionClass {
		initHandler( alias ).getDirectMessages()
	}

	/**
	 * Get list of your sent direct messgaes<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/DirectMessagesResources.html#getSentDirectMessages()">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @return returns list of direct messages sent by user
	 * @throws TwitterExceptionClass
	 */
	static List<DirectMessage> twitterGetSentDirectMessages( String alias ) throws TwitterExceptionClass {
		initHandler( alias ).getSentDirectMessages()
	}

	/**
	 * Get specified direct message using direct message id<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/DirectMessagesResources.html#showDirectMessage(int)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param directMessageId distinguishing string of a direct message
	 * @return returns the specified direct message
	 * @throws TwitterExceptionClass
	 */
	static DirectMessage twitterShowDirectMessage( String alias, long directMessageId)
	throws TwitterExceptionClass {
		initHandler( alias ).showDirectMessage( directMessageId )
	}

	/**
	 * Delete direct messages<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/DirectMessagesResources.html#destroyDirectMessage(long)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param directMessageId distinguishing string of a direct message
	 * @return returns information about deleted direct message
	 * @throws TwitterExceptionClass
	 */
	static DirectMessage twitterDestroyDirectMessage( String alias, long directMessageId)
	throws TwitterExceptionClass {
		initHandler( alias ).destroyDirectMessage( directMessageId )
	}

	/**
	 * Send a direct message in Twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/DirectMessagesResources.html#sendDirectMessage(long,%20java.lang.String)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param userID is the unique identifying numberss for a user
	 * @param message is the  text or content to be sent to the user
	 * @return returns information about the sent direct message
	 * @throws TwitterExceptionClass
	 */
	static DirectMessage twitterSendDirectMessage( String alias, long userID, String message )
	throws TwitterExceptionClass {
		initHandler( alias ).sendDirectMessage( userID, message )
	}

	//Friends and Followers Resources

	/**
	 * Get friends user id's<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/FriendsFollowersResources.html#getFriendsIDs(long)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param cursor breaks response into pages
	 * @return returns a list of the users friends IDs
	 * @throws TwitterExceptionClass
	 */
	static Object twitterGetFriendsIDs( String alias, long cursor) throws TwitterExceptionClass {
		initHandler( alias ).getFriendsIDs( cursor )
	}

	/**
	 * Get followers user id's<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/FriendsFollowersResources.html#getFollowersIDs(long)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param cursor is the list limit
	 * @return returns a list of the users follower
	 * @throws TwitterExceptionClass
	 */
	static Object twitterGetFollowersIDs( String alias, long cursor ) throws TwitterExceptionClass {
		initHandler( alias ).getFollowersIDs( cursor )
	}

	/**
	 * List of suggested friends on twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/FriendsFollowersResources.html#lookupFriendships(java.lang.String[])">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param id is the identification string of the friend/follower
	 * @return returns the friendship information about the friend/follower
	 * @throws TwitterExceptionClass
	 */
	static ResponseList<Friendship> twitterLookupFriendships( String alias, String... id ) throws TwitterExceptionClass {
		initHandler( alias ).lookupFriendships( id )
	}

	/**
	 * Get friend request on twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/FriendsFollowersResources.html#getIncomingFriendships(long)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param cursor is the list limit
	 * @return returns a list of user IDs
	 * @throws TwitterExceptionClass
	 */
	static IDs twitterGetIncomingFriendships( String alias, long cursor ) throws TwitterExceptionClass {
		initHandler( alias ).getIncomingFriendships( cursor )
	}

	/**
	 * Get list of outging friends on twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/FriendsFollowersResources.html#getOutgoingFriendships(long)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param cursor is the list limit
	 * @return returns a list of user IDs
	 * @throws TwitterExceptionClass
	 */
	static IDs twitterGetOutgoingFriendship( String alias, long cursor ) throws TwitterExceptionClass {
		initHandler( alias ).getOutgoingFriendships( cursor )
	}

	/**
	 * Create friendship on twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/FriendsFollowersResources.html#createFriendship(long)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param userID is the identifying numbers of an user
	 * @return returns information about created friendship
	 * @throws TwitterExceptionClass
	 */
	static User twitterCreateFriendship( String alias, long userID ) throws TwitterExceptionClass {
		initHandler( alias ).createFriendship( userID )
	}

	/**
	 * Delete friendship<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/FriendsFollowersResources.html#destroyFriendship(long)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param userID is the identifying numbers of an user
	 * @return returns info about deleted friendship
	 * @throws TwitterExceptionClass
	 */
	static User twitterDestroyFriendship( String alias, long userID ) throws TwitterExceptionClass {
		initHandler( alias ).destroyFriendship( userID )
	}

	/**
	 * Update friendship on twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/FriendsFollowersResources.html#updateFriendship(long,boolean,boolean)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param userID is the identifying numbers of an user
	 * @param enableDeviceNotification boolean to enable notification
	 * @param retweets boolean to enable/disable retweets
	 * @return returns information about updated relationship
	 * @throws TwitterExceptionClass
	 */
	static Relationship twitterUpdateFriendship( String alias, long userID, boolean enableDeviceNotification,
			boolean retweets ) throws TwitterExceptionClass {
		initHandler( alias ).updateFriendship( userID, enableDeviceNotification, retweets)
	}

	/**
	 * Show friendship in twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/FriendsFollowersResources.html#showFriendship(long,%20long)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param sourceName is the screen name for the source
	 * @param targetName is the screen name for the target
	 * @return returns friendship information
	 * @throws TwitterExceptionClass
	 */
	static Relationship twitterShowFriendship( String alias,
			String sourceName, String targetName ) throws TwitterExceptionClass {
		initHandler( alias ).showFriendship( sourceName, targetName )
	}

	/**
	 * Get list of friend in twitter<br/>
	 * http://twitter4j.org/javadoc/twitter4j/api/FriendsFollowersResources.html#getFriendsList(long,%20long)
	 * @param alias name on toroproperties credentials
	 * @param screenName is the screen name for the source
	 * @param cursor is the list limit
	 * @return returns friends list of the specified user
	 * @throws TwitterExceptionClass
	 */
	static PagableResponseList<User> twitterGetFriendsList( String alias,
			String screenName, long cursor )throws TwitterExceptionClass {
		initHandler( alias ).getFriendsList( screenName, cursor )
	}

	/**
	 * Get a list of followers on twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/FriendsFollowersResources.html#getFollowersList(long,%20long)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param screenName is the screen name for the source
	 * @param cursor is the list limit
	 * @return returns followers list of the specified user
	 * @throws TwitterExceptionClass
	 */
	static PagableResponseList<User> twitterGetFollowersList( String alias,
			String screenName, long cursor )throws TwitterExceptionClass {
		initHandler( alias ).getFollowersList( screenName, cursor )
	}

	//Users Resources

	/**
	 * Get your account settings on twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/UsersResources.html#getAccountSettings()">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @return returns user account settings
	 * @throws TwitterExceptionClass
	 */
	static AccountSettings twitterGetAccountSettings( String alias ) throws TwitterExceptionClass {
		initHandler( alias ).getAccountSettings()
	}

	/**
	 * Verify credentials on twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/UsersResources.html#verifyCredentials()">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @return returns information about the user
	 * @throws TwitterExceptionClass
	 */
	static User twitterVerifyCredentials( String alias ) throws TwitterExceptionClass {
		initHandler( alias ).verifyCredentials()
	}

	/**
	 * Update your account settings in twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/UsersResources.html#updateAccountSettings(java.lang.Integer,%20java.lang.Boolean,%20java.lang.String,%20java.lang.String,%20java.lang.String,%20java.lang.String)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param trend users default trend location
	 * @param sleepTimeEnabled enable/disable sleep time
	 * @param startSleepTime sleep time
	 * @param endSleepTime end of sleep time
	 * @param timeZone user set date and timezone
	 * @param lang language of twitter
	 * @return returns information about updated account
	 * @throws TwitterExceptionClass
	 */
	static AccountSettings twitterUpdateAccountSettings( String alias, int trend,
			boolean sleepTimeEnabled, String startSleepTime, String endSleepTime,
			String timeZone, String lang ) throws TwitterExceptionClass {
		initHandler( alias ).updateAccountSettings( trend, sleepTimeEnabled,
				startSleepTime, endSleepTime, timeZone, lang )
	}

	/**
	 * Update user profile on twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/UsersResources.html">See External Resources Here</a>
	 * #updateProfile(java.lang.String,%20java.lang.String,%20java.lang.String,%20java.lang.String)
	 * @param alias name on toroproperties credentials
	 * @param name name of the user
	 * @param url of the account
	 * @param location of the user
	 * @param description of the account
	 * @return returns information about update user
	 * @throws TwitterExceptionClass
	 */
	static User twitterUpdateProfile(String alias, String name, String url,
			String location, String description) throws TwitterExceptionClass {
		initHandler( alias ).updateProfile( name, url, location, description )
	}

	/**
	 * Update user profile background image on twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/UsersResources.html#updateProfileBackgroundImage(java.io.File,%20boolean)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param file object of the photo
	 * @param tile boolean to be tiled
	 * @return returns information about the updated profile
	 * @throws TwitterExceptionClass
	 */
	static User twitterUpdateProfileBackgroundImage( String alias, File file, boolean tile ) throws TwitterExceptionClass {
		initHandler( alias ).updateProfileBackgroundImage( file, tile )
	}

	/**
	 * Update user profile colors on twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/UsersResources.html#updateProfileColors(java.lang.String,%20java.lang.String,%20java.lang.String,%20java.lang.String,%20java.lang.String)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param bckColor background color
	 * @param txtColor color of texts
	 * @param linkColor color of links
	 * @param sidebarFillColor side bar color
	 * @param sidebarBorderColor side bar border color
	 * @return returns information about updated user profile
	 * @throws TwitterExceptionClass
	 */
	static User twitterUpdateProfileColors( String alias, String bckColor,
			String txtColor, String linkColor, String sidebarFillColor,
			String sidebarBorderColor ) throws TwitterExceptionClass {
		initHandler( alias ).updateProfileColors( bckColor, txtColor, linkColor, sidebarFillColor, sidebarBorderColor )
	}

	/**
	 * Update user profile image on twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/UsersResources.html#updateProfileImage(java.io.File)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param file object of the photo
	 * @return returns information about the updated profiles
	 * @throws TwitterExceptionClass
	 */
	static User twitterUpdateProfileImage( String alias, File file ) throws TwitterExceptionClass {
		initHandler( alias ).updateProfileImage( file )
	}

	/**
	 * Get user blocklist on twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/UsersResources.html#getBlocksList()">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @return returns list of blocked users
	 * @throws TwitterExceptionClass
	 */
	static List<User> twitterGetBlocksList( String alias ) throws TwitterExceptionClass {
		initHandler( alias ).getBlocksList()
	}

	/**
	 * Get blocked users id on twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/UsersResources.html#getBlocksIDs()">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @return returns a list of blocked user IDs
	 * @throws TwitterExceptionClass
	 */
	static IDs twitterGetBlocksIDs( String alias ) throws TwitterExceptionClass {
		initHandler( alias ).getBlocksIDs()
	}

	/**
	 * Create a blocked user on twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/UsersResources.html#createBlock(long)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param scrnName is the unique identifying name for a user
	 * @return returns information about blocked user
	 * @throws TwitterExceptionClass
	 */
	static User twitterCreateBlock( String alias, String scrnName ) throws TwitterExceptionClass {
		initHandler( alias ).createBlock( scrnName )
	}

	/**
	 * Delete user from blocked list<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/UsersResources.html#destroyBlock(long)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param scrnName is the unique identifying name for a user
	 * @return returns information about unblocked user
	 * @throws TwitterExceptionClass
	 */
	static User twitterDestroyBlock( String alias, String scrnName ) throws TwitterExceptionClass {
		initHandler( alias ).destroyBlock( scrnName )
	}

	/**
	 * Get other users on twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/UsersResources.html#lookupUsers(long[])">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param scrnName is the unique identifying name for a user
	 * @return returns information about users
	 * @throws TwitterExceptionClass
	 */
	static List<User> twitterLookupUsers( String alias, String... scrnName ) throws TwitterExceptionClass {
		initHandler( alias ).lookupUsers( scrnName )
	}

	/**
	 * Get specified user on twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/UsersResources.html#showUser(long)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param scrnName is the unique identifying name for a user
	 * @return returns information about the user
	 * @throws TwitterExceptionClass
	 */
	static User twitterShowUser( String alias, String scrnName ) throws TwitterExceptionClass {
		initHandler( alias ).showUser( scrnName )
	}

	/**
	 * Search for other users on twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/UsersResources.html#searchUsers(java.lang.String,%20int)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param query question to run search
	 * @param page number of users per search
	 * @return returns a list of users
	 * @throws TwitterExceptionClass
	 */
	static List<User> twitterSearchUsers( String alias, String query, int page ) throws TwitterExceptionClass {
		initHandler( alias ).searchUsers( query, page )
	}

	/**
	 * Get contributees on twitter development<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/UsersResources.html#getContributees(long)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param scrnName is the unique identifying name for a user
	 * @return returns list of contributors
	 * @throws TwitterExceptionClass
	 */
	static List<User> twitterGetContributees( String alias, String scrnName ) throws TwitterExceptionClass {
		initHandler( alias ).getContributees( scrnName )
	}

	/**
	 * Get contributors on twitter development<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/UsersResources.html#getContributors(long)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param scrnName is the unique identifying name for a user
	 * @return returns a list of contributors
	 * @throws TwitterExceptionClass
	 */
	static List<User> twitterGetContributors( String alias, String scrnName ) throws TwitterExceptionClass {
		initHandler( alias ).getContributors( scrnName )
	}

	/**
	 * Delete profile banner on twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/UsersResources.html#removeProfileBanner()">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @throws TwitterExceptionClass
	 */
	static void twitterRemoveProfileBanner( String alias ) throws TwitterExceptionClass {
		initHandler( alias ).removeProfileBanner( )
	}

	/**
	 * Update profile banner on twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/UsersResources.html#updateProfileBanner(java.io.File)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param file is the image file to be used as banner
	 * @throws TwitterExceptionClass
	 */
	static void twitterUpdateProfileBanner( String alias, File file ) throws TwitterExceptionClass {
		initHandler( alias ).updateProfileBanner( file )
	}

	/**
	 * Create mute ont twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/UsersResources.html#createMute-long-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param scrnName is the unique identifying name for a user
	 * @return returns the muted user information
	 * @throws TwitterExceptionClass
	 */
	static User twitterCreateMute( String alias, String scrnName ) throws TwitterExceptionClass {
		initHandler( alias ).createMute( scrnName )
	}

	/**
	 * Delete mute on twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/UsersResources.html#destroyMute-long-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param scrnName is the unique identifying name for a user
	 * @return returns information about deleted mute
	 * @throws TwitterExceptionClass
	 */
	static User twitterDestroyMute( String alias, String scrnName ) throws TwitterExceptionClass {
		initHandler( alias ).destroyMute( scrnName )
	}

	/**
	 * Get mute id's on twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/UsersResources.html#getMutesIDs-long-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param cursor the number limit of the list
	 * @return returns a list of muted user IDs
	 * @throws TwitterExceptionClass
	 */
	static IDs twitterGetMutesIDs( String alias, long cursor ) throws TwitterExceptionClass {
		initHandler( alias ).getMutesIDs( cursor )
	}

	/**
	 * Get mute list on twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/UsersResources.html#getMutesList-long-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param cursor the number limit of the list
	 * @return returns a list of muted users
	 * @throws TwitterExceptionClass
	 */
	static List<User> twitterGetMutesList( String alias, long cursor ) throws TwitterExceptionClass {
		initHandler( alias ).getMutesList( cursor )
	}

	//Suggested Users Resources

	/**
	 * Get other user suggestions on twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/SuggestedUsersResources.html#getUserSuggestions-java.lang.String-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param slug hort name of list or a category
	 * @return returns list of user suggestions
	 * @throws TwitterExceptionClass
	 */
	static List<User> twitterGetUserSuggestions( String alias, String slug ) throws TwitterExceptionClass {
		initHandler( alias ).getUserSuggestions( slug )
	}

	/**
	 * Get user categories on twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/SuggestedUsersResources.html#getSuggestedUserCategories--">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @return returns list of categories
	 * @throws TwitterExceptionClass
	 */
	static ResponseList<Category> twitterGetSuggestedUserCategories( String alias ) throws TwitterExceptionClass {
		initHandler( alias ).getSuggestedUserCategories()
	}

	/**
	 * Get a list of member suggestions on twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/SuggestedUsersResources.html#getMemberSuggestions-java.lang.String-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param slug hort name of list or a category
	 * @return returns list of users under the slug
	 * @throws TwitterExceptionClass
	 */
	static List<User> twitterGetMemberSuggestions( String alias, String slug ) throws TwitterExceptionClass {
		initHandler( alias ).getMemberSuggestions( slug )
	}

	//Favorites Resources

	/**
	 * Get favorite tweets<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/FavoritesResources.html#getFavorites()">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @return returns list of favorite status
	 * @throws TwitterExceptionClass
	 */
	static List<Status> twitterGetFavorites( String alias ) throws TwitterExceptionClass {
		initHandler( alias ).getFavorites()
	}

	/**
	 * Remove status from being favorited on twitter<br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/FavoritesResources.html#destroyFavorite(long)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param id the identification number of the status
	 * @return returns information about deleted favorite
	 * @throws TwitterExceptionClass
	 */
	static Status twitterDestroyFavorite( String alias, long id ) throws TwitterExceptionClass {
		initHandler( alias ).destroyFavorite( id )
	}

	/**
	 * Favorite a specific status or tweet on twitter <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/FavoritesResources.html#createFavorite(long)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param id the identification number of the status
	 * @return returns information about favorite status
	 * @throws TwitterExceptionClass
	 */
	static Status twitterCreateFavorite( String alias, long id ) throws TwitterExceptionClass {
		initHandler( alias ).createFavorite( id )
	}

	//List Resources

	/**
	 * Get a list users on twitter <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/ListsResources.html#getUserLists-long-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param scrnName identifying string of the user
	 * @return returns user ids
	 * @throws TwitterExceptionClass
	 */
	static List<UserList> twitterGetUserLists( String alias, String scrnName ) throws TwitterExceptionClass {
		initHandler( alias ).getUserLists( scrnName )
	}

	/**
	 * Get other users list of statuses <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/ListsResources.html#getUserListStatuses-long-twitter4j.Paging-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param listID identifiying number of list
	 * @param page object that controls pagination
	 * @return returns list of statuses of the list
	 * @throws TwitterExceptionClass
	 */
	static ResponseList<Status> twitterGetUserListStatuses( String alias,
			long listID, Paging page ) throws TwitterExceptionClass {
		initHandler( alias ).getUserListStatuses( listID, page )
	}

	/**
	 * Destroy list of user member <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/ListsResources.html#destroyUserListMember-long-java.lang.String-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param listID identifiying number of list
	 * @param scrnName identifying string of the user
	 * @return returns the update list
	 * @throws TwitterExceptionClass
	 */
	static UserList twitterDestroyUserListMember( String alias, long listID,
			String scrnName ) throws TwitterExceptionClass {
		initHandler( alias ).destroyUserListMember( listID, scrnName )
	}

	/**
	 * Get a list of user membership <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/ListsResources.html#getUserListMemberships-int-long-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param scrnName the identification name of the list member
	 * @param cursor breaks the result into pages
	 * @return returns a list of list
	 * @throws TwitterExceptionClass
	 */
	static List<UserList> twitterGetUserListMemberships( String alias,
			String scrnName, long cursor ) throws TwitterExceptionClass {
		initHandler( alias ).getUserListMemberships( scrnName, cursor )
	}

	/**
	 * Get user subscriber list <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/ListsResources.html#getUserListSubscribers-long-int-long-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param listID identifiying number of list
	 * @param cursor breaks the result into pages
	 * @return returns list of list
	 * @throws TwitterExceptionClass
	 */
	static PagableResponseList<User> twitterGetUserListSubscribers( String alias,
			long listID, long cursor ) throws TwitterExceptionClass {
		initHandler( alias ).getUserListSubscribers( listID, cursor )
	}

	/**
	 * Create a list of user subscription <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/ListsResources.html#createUserListSubscription(int)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param listID identifiying number of list
	 * @return returns the updated list
	 * @throws TwitterExceptionClass
	 */
	static UserList twitterCreateUserListSubscription( String alias, long listID ) throws TwitterExceptionClass {
		initHandler( alias ).createUserListSubscription( listID )
	}

	/**
	 * Get list of list of user subscription <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/ListsResources.html#showUserListSubscription-long-long-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param listID identifiying number of list
	 * @param userID dentifiying number of the user
	 * @return returns information about the updated list
	 * @throws TwitterExceptionClass
	 */
	static User twitterShowUserListSubscription( String alias, long listID, long userID ) throws TwitterExceptionClass {
		initHandler( alias ).showUserListSubscription( listID, userID )
	}

	/**
	 * Delete user's list of subscription <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/ListsResources.html#destroyUserListSubscription(int)">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param listID identifiying number of list
	 * @return returns infromation about destroyed list
	 * @throws TwitterExceptionClass
	 */
	static UserList twitterDestroyUserListSubscription( String alias, long listID ) throws TwitterExceptionClass {
		initHandler( alias ).destroyUserListSubscription( listID )
	}

	/**
	 * Get list of members in twitter <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/ListsResources.html#createUserListMembers-long-java.lang.String...-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param listID identifiying number of list
	 * @param usersScrnName is an array of screen names
	 * @return returns information about created subscription
	 * @throws TwitterExceptionClass
	 */
	static UserList twitterCreateUserListMembers( String alias, long listID,
			String... usersScrnName ) throws TwitterExceptionClass {
		initHandler( alias ).createUserListMembers( listID, usersScrnName )
	}

	/**
	 * Get lisr of user membership <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/ListsResources.html#showUserListMembership-long-long-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param listID identifiying number of list
	 * @param userID dentifiying number of the user
	 * @return returns information about user
	 * @throws TwitterExceptionClass
	 */
	static User twitterShowUserListMembership( String alias, long listID, long userID ) throws TwitterExceptionClass {
		initHandler( alias ).showUserListMembership( listID, userID )
	}

	/**
	 * Get a list of user members <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/ListsResources.html#getUserListMembers-long-int-long-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param listID identifiying number of list
	 * @param cursor breaks the result into pages
	 * @return returns list of information about user
	 * @throws TwitterExceptionClass
	 */
	static PagableResponseList<User> twitterGetUserListMembers( String alias,
			long listID, long cursor ) throws TwitterExceptionClass {
		initHandler( alias ).getUserListMembers( listID, cursor )
	}

	/**
	 * Create a list of members <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/ListsResources.html#createUserListMember-long-long-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param listID identifiying number of list
	 * @param userID identifying numbers of a user
	 * @return returns information about users
	 * @throws TwitterExceptionClass
	 */
	static UserList twitterCreateUserListMember( String alias, long listID, long userID ) throws TwitterExceptionClass {
		initHandler( alias ).createUserListMember( listID, userID )
	}

	/**
	 * Create a user list on twitter <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/ListsResources.html#createUserList-java.lang.String-boolean-java.lang.String-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param listName identifying name of the list
	 * @param publicList enable the list to be public or not
	 * @param desc description of the list
	 * @return returns information about the list
	 * @throws TwitterExceptionClass
	 */
	static UserList twitterCreateUserList( String alias, String listName,
			boolean publicList, String desc ) throws TwitterExceptionClass {
		initHandler( alias ).createUserList( listName, publicList, desc )
	}

	/**
	 * Update a user list on twitter <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/ListsResources#updateUserList-long-java.lang.String-boolean-java.lang.String-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param listID identifying number of a list
	 * @param listName identifying name of the list
	 * @param publicList enable the list to be public or not
	 * @param desc description of the list
	 * @return returns information about the list
	 * @throws TwitterExceptionClass
	 */
	static UserList twitterUpdateUserList( String alias, long listID, String listName,
			boolean publicList, String desc ) throws TwitterExceptionClass {
		initHandler( alias ).updateUserList( listID , listName, publicList, desc )
	}

	/**
	 * Delete a user list on twitter <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/ListsResources.html#destroyUserList-long-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param listID identifying number of a list
	 * @return returns information about the list
	 * @throws TwitterExceptionClass
	 */
	static UserList twitterDestroyUserList( String alias, long listID ) throws TwitterExceptionClass {
		initHandler( alias ).destroyUserList( listID )
	}

	/**
	 * Get user list on twitter <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/ListsResources.html#showUserList-long-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param id identifying numbers of the user
	 * @return returns user's lists
	 * @throws TwitterExceptionClass
	 */
	static UserList twitterShowUserList( String alias, long id ) throws TwitterExceptionClass {
		initHandler( alias ).showUserList( id )
	}

	/**
	 * Get users list of subscriptions on twitter <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/ListsResources.html#getUserListSubscriptions-long-int-long-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param scrnName is the unique identifying name for a user
	 * @param cursor breaks response into pages
	 * @return returns list of urer list
	 * @throws TwitterExceptionClass
	 */
	static PagableResponseList<UserList> twitterGetUserListSubscriptions( String alias,
			String scrnName, long cursor ) throws TwitterExceptionClass {
		initHandler( alias ).getUserListSubscriptions( scrnName, cursor )
	}

	//Saved Searches Resources

	/**
	 * Get saved searched on twitter <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/SavedSearchesResources.html#getSavedSearches--">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @return returns an array of numeric ids of searches
	 * @throws TwitterExceptionClass
	 */
	static List<SavedSearch> twitterGetSavedSearches( String alias ) throws TwitterExceptionClass {
		initHandler( alias ).getSavedSearches()
	}

	/**
	 * Create a saved search on twitter
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/SavedSearchesResources.html#createSavedSearch-java.lang.String-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param query the question to be asked and saved
	 * @return returns information about saved query
	 * @throws TwitterExceptionClass
	 */
	static SavedSearch twitterCreateSavedSearch( String alias, String query ) throws TwitterExceptionClass {
		initHandler( alias ).createSavedSearch( query )
	}

	/**
	 * Show saved searches on twitter <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/SavedSearchesResources.html#showSavedSearch-long-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param id identifying number of the query
	 * @return returns information about the specified query
	 * @throws TwitterExceptionClass
	 */
	@TypeChecked( TypeCheckingMode.SKIP )
	static SavedSearch twitterShowSavedSearch( String alias, Long id ) throws TwitterExceptionClass {
		initHandler( alias ).showSavedSearch( id )
	}

	/**
	 * Delete saved searches <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/SavedSearchesResources.html#destroySavedSearch-long-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param id identifying number of the query
	 * @return returns information about the specified query
	 * @throws TwitterExceptionClass
	 */
	@TypeChecked( TypeCheckingMode.SKIP )
	static SavedSearch twitterDestroySavedSearch( String alias, Long id ) throws TwitterExceptionClass {
		initHandler( alias ).destroySavedSearch( id )
	}

	//Places and Geo Resources

	/**
	 * Get geographic details on twitter <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/PlacesGeoResources.html#getGeoDetails-java.lang.String-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param placeId place identification string
	 * @return returns information about place
	 * @throws TwitterExceptionClass
	 */
	static Place twitteretGeoDetails( String alias, String placeId ) throws TwitterExceptionClass {
		initHandler( alias ).getGeoDetails( placeId )
	}

	/**
	 * Get a reverse geographical code on twitter <br/>
	 * http://twitter4j.org/javadoc/twitter4j/api/PlacesGeoResources.html#reverseGeoCode-twitter4j.GeoQuery-
	 * @param alias name on toroproperties credentials
	 * @param query is the coordinates of the place
	 * @return returns list of information about the place
	 * @throws TwitterExceptionClass
	 */
	static ResponseList<Place> twitterReverseGeoCode( String alias, GeoQuery query ) throws TwitterExceptionClass {
		initHandler( alias ).reverseGeoCode( query )
	}

	/**
	 * Search for places on twitter <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/PlacesGeoResources.html#searchPlaces-twitter4j.GeoQuery-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param query is the coordinates, or ip address of the place
	 * @return returns list of information about the place
	 * @throws TwitterExceptionClass
	 */
	static ResponseList<Place> twitterSearchPlaces( String alias, GeoQuery query ) throws TwitterExceptionClass {
		initHandler( alias ).searchPlaces( query )
	}

	/**
	 * Get simmilar places on twitter <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/PlacesGeoResources.html#getSimilarPlaces-twitter4j.GeoLocation-java.lang.String-java.lang.String-java.lang.String-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param loc the coordinates of the place
	 * @param name the name of the place
	 * @param containedWithin restricts the searxh within the place
	 * @param streetAddress the specific street address of the place
	 * @return returns a list of information about place
	 * @throws TwitterExceptionClass
	 */
	static ResponseList<Place> twitterGetSimilarPlaces( String alias, GeoLocation loc, String name,
			String containedWithin, String streetAddress ) throws TwitterExceptionClass {
		initHandler( alias ).getSimilarPlaces( loc, name, containedWithin, streetAddress )
	}

	//Trends Resources

	/**
	 * Get trends by place on twitter <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/TrendsResources.html#getPlaceTrends-int-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param woeID where on earth ID
	 * @return returns a list of trends
	 * @throws TwitterExceptionClass
	 */
	static Trends twitterGetPlaceTrends( String alias, int woeID ) throws TwitterExceptionClass {
		initHandler( alias ).getPlaceTrends( woeID )
	}

	/**
	 * Get available trends on twitter <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/TrendsResources.html#getAvailableTrends--">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @return returns a list of trends by location
	 * @throws TwitterExceptionClass
	 */
	static ResponseList<Location> twittergGetAvailableTrends( String alias ) throws TwitterExceptionClass {
		initHandler( alias ).getAvailableTrends()
	}

	/**
	 * Get closest trends on twitter <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/TrendsResources.html#getClosestTrends-twitter4j.GeoLocation-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param geo the coordinates of the place
	 * @return returns a list of trends by location
	 * @throws TwitterExceptionClass
	 */
	static ResponseList<Location> twitterGetClosestTrends( String alias, GeoLocation geo ) throws TwitterExceptionClass {
		initHandler( alias ).getClosestTrends( geo )
	}

	//Spam Reporting Resource

	/**
	 * Report spam on twitter <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/SpamReportingResource.html#reportSpam-java.lang.String-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param scrnName is the user name of the user
	 * @return returns information
	 * @throws TwitterExceptionClass
	 */
	static User twitterReportSpam( String alias, String scrnName ) throws TwitterExceptionClass {
		initHandler( alias ).reportSpam( scrnName )
	}

	//Help Resources

	/**
	 * Get API Configuration <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/HelpResources.html#getAPIConfiguration--">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @return returns users configuration on twitter
	 * @throws TwitterExceptionClass
	 */
	static TwitterAPIConfiguration twitterGetAPIConfiguration( String alias ) throws TwitterExceptionClass {
		initHandler( alias ).getAPIConfiguration()
	}

	/**
	 * Get languages of twitter <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/HelpResources.html#getLanguages--">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @return returns list of languages supported by Twitter
	 * @throws TwitterExceptionClass
	 */
	static ResponseList<Language> twitterGetLanguages( String alias ) throws TwitterExceptionClass {
		initHandler( alias ).getLanguages()
	}

	/**
	 * Get privacy policy on twitter <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/HelpResources.html#getPrivacyPolicy--">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @return returns privacy policy
	 * @throws TwitterExceptionClass
	 */
	static String twitterGetPrivacyPolicy( String alias ) throws TwitterExceptionClass {
		initHandler( alias ).getPrivacyPolicy()
	}

	/**
	 * Get twitter terms of service <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/HelpResources.html#getTermsOfService--">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @return returns terms of service
	 * @throws TwitterExceptionClass
	 */
	static String twitterGetTermsOfService( String alias ) throws TwitterExceptionClass {
		initHandler( alias ).getTermsOfService()
	}

	/**
	 * Get rate limit status <br/>
	 * <a href="http://twitter4j.org/javadoc/twitter4j/api/HelpResources.html#getRateLimitStatus-java.lang.String...-">See External Resources Here</a>
	 * @param alias name on toroproperties credentials
	 * @param resources are twitter services
	 * @return map of calls left
	 * @throws TwitterExceptionClass
	 */
	static Map<String,RateLimitStatus> twitterGetRateLimitStatus( String alias,
			String... resources ) throws TwitterExceptionClass {
		initHandler( alias ).getRateLimitStatus( resources )
	}
}
