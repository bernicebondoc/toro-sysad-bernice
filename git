#!/usr/bin/env groovy
//version updater script
//usage: ./version minor|major|patch|current|develop|custom save

def currentDirectory = "pwd".execute()
def currentPath = "${currentDirectory.text.trim()}/pom.xml";

//println "$currentPath"

firstarg = currentPath

//check thirdArgument has value
try {
	secondarg = args[0]
}
catch ( ArrayIndexOutOfBoundsException e ) {
	secondarg = 'help'
}

try {
	thirdArg = args[1]
}
catch ( ArrayIndexOutOfBoundsException e ) {
	thirdArg = 'no'
}


void checkVersion(String pomLocation) {

	def saveWarning = "Use 'save' as second argument to update version"

	//assign parent pom to a different variable to preserve parent version
	//parent pom version
	def parentPomLocation = firstarg
	//get parent pom file
	def parentpomFile = new File(parentPomLocation).getAbsoluteFile()
	//parse parent XML
	def parentpom = new XmlParser().parse(parentpomFile)
	//parent Version
	def parentVersion = parentpom.version.text()


	//get pom file
	def pomFile = new File(pomLocation).getAbsoluteFile()
	//parse XML
	def pom = new XmlParser().parse(pomFile)
	//extract version
	def versionString = pom.version.text()
    if (!versionString)
        versionString = pom.parent.version.text()
	
	def (major, minor, patch) = ( versionString.contains('-') ? versionString.tokenize('-')[ 0 ] : versionString ).tokenize('.')
	def (oldVersion, meta) = versionString.tokenize( '-' )

	def newVersion

    //second argument defines what to execute    
    switch(secondarg) {
    	case ['c','current']:
	    	//prints current version
			println "$versionString \t $pomFile"
				//is the pom a parent of a multi-module project?
				//if yes, check the child poms
				if (pom.packaging.text() == 'pom') {
				    //println 'Checking children modules'
				    for (int i = 0; i < pom.modules.module.size(); i++) {
				        def location = "${pomFile.getAbsoluteFile().getParent()}/${pom.modules.module[i].text()}/pom.xml"
				        //println location
				        if (new File(location).exists())
				            checkVersion(location)
				    }
				    return
				}	
	    	break
    	case ['major']:
    		major = major.toInteger() + 1
			minor = 0
	    	//prints version and increment major
	    	newVersion = "$major.$minor"
			println "New Version is $newVersion. $saveWarning"
			//if user passes save, execute maven repository update
			saveNewVersion(pom, newVersion, parentVersion, thirdArg)
	    	break
		case ['major-meta']:
    		major = major.toInteger() + 1
			minor = 0
	    	//prints version and increment major
	    	newVersion = "$major.$minor-SNAPSHOT"
			println "New Version is $newVersion. $saveWarning"
			//if user passes save, execute maven repository update
			saveNewVersion(pom, newVersion, parentVersion, thirdArg)
	    	break
	    case ['minor']:
	    	minor = minor.toInteger() + 1
	    	//prints version and increment minor
	    	newVersion = "$major.$minor"
			println "New Version is $newVersion. $saveWarning"
			//if user passes save, execute maven repository update
			saveNewVersion(pom, newVersion, parentVersion, thirdArg)
	    	break
	    case ['minor-meta']:
	    	minor = minor.toInteger() + 1
	    	//prints version and increment minor
	    	newVersion = "$major.$minor-SNAPSHOT"
			println "New Version is $newVersion. $saveWarning"
			//if user passes save, execute maven repository update
			saveNewVersion(pom, newVersion, parentVersion, thirdArg)
	    	break
	    case ['patch']:
	    	try {
				patch = patch.toInteger() + 1
			}
			catch ( NullPointerException e ) {
				patch = 1
			}
	    	//prints version and increment patch
	    	newVersion = "$major.$minor.$patch"
			println "New Version is $newVersion. $saveWarning"
			//if user passes save, execute maven repository update
			saveNewVersion(pom, newVersion, parentVersion, thirdArg)
	    	break
	    case ['patch-meta']:
	    	try {
				patch = patch.toInteger() + 1
			}
			catch ( NullPointerException e ) {
				patch = 1
			}
	    	//prints version and increment patch
	    	newVersion = "$major.$minor.$patch-SNAPSHOT"
			println "New Version is $newVersion. $saveWarning"
			//if user passes save, execute maven repository update
			saveNewVersion(pom, newVersion, parentVersion, thirdArg)
	    	break
	    case ['rem-meta']:
	    	//prints version and increment patch
	    	newVersion = "$oldVersion"
			println "New Version is $newVersion. $saveWarning"
			//if user passes save, execute maven repository update
			saveNewVersion(pom, newVersion, parentVersion, thirdArg)
	    	break
	    case ['custom']:
	    	newVersion = "$thirdArg"
			println "New Version is $newVersion. $saveWarning"
			//if user passes save, execute maven repository update
			saveNewVersion(pom, newVersion, parentVersion, 'save')
	    	break
	   default:  
            println "This script will help you update a maven project's version"
            println "Based on Semantic Versioning. Maven automatically checks your pom.xml for"
            println "child modules and updates all the versions accordingly."
            println "Usage:"
            println "./version current \t\t\t\t prints the current version of your project"
            println "./version major \t\t\t\t outputs the new version with the major digit incremented"
            println "./version minor \t\t\t\t outputs the new version with the minor digit incremented"
            println "./version patch \t\t\t\t outputs the new version with the patch digit incremented"
            println "./version major-meta \t\t\t\t outputs the new version with the major digit incremented with SNAPSHOT meta"
            println "./version minor-meta \t\t\t\t outputs the new version with the minor digit incremented with SNAPSHOT meta"
            println "./version patch-meta \t\t\t\t outputs the new version with the patch digit incremented with SNAPSHOT meta"
            println "./version rem-meta \t\t\t\t outputs the current version without a SNAPSHOT meta"
            println "./version custom \t\t\t\t gets your custom new version and saves to project"
            println "./version [option] save \t\t\t saves the new generated version to project"
    }


}

//method to save new version to file
void saveNewVersion(def pom, String version, def parentVersion, String saveNew){

	
if (saveNew == 'save'){	
	//if pom is a child pom, use parent version and update child pom.xml
	def rawPom = pom.version
	if (!rawPom){
		rawPom = pom.parent.version
		version = parentVersion
	}

	rawPom.each {
		it.value = version
	}
	def updateVersion = "mvn versions:set -DnewVersion=$version -q".execute()
	println updateVersion.text
	def commitVersion = "mvn versions:commit -q".execute()
	println commitVersion.text
	//secondarg = 'current'
	//checkVersion(firstarg)
	println "Version should be $version now."
}

}

checkVersion(firstarg)
