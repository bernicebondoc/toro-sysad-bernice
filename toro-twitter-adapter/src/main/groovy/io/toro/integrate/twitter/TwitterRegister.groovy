package io.toro.integrate.twitter

import javax.servlet.http.HttpServletRequest
import javax.ws.rs.DefaultValue

import org.hibernate.validator.constraints.NotBlank
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

import twitter4j.Twitter
import twitter4j.TwitterFactory
import twitter4j.auth.AccessToken
import twitter4j.auth.RequestToken

import com.toro.esb.core.service.annotation.ApiIgnore
@RequestMapping( 'twitter' )
public class TwitterRegister {

	/**
	 * This service is used to save your Twitter details to the ESB. If you export/import this package, any saved Twitter
	 * account details will be exported/imported also.
	 * @param alias The alias to use for this account
	 * @param redirectUri the redirect url
	 * @param consumerKey identifies your application when making requests to the API. It is used to securely sign your requests to Twitter.
	 * @param consumerSecret A secret used by the consumer to establish ownership of the consumer key and also used to securely sign your requests to Twitter.
	 * @param request request access to Twitter Auth
	 * @return
	 */
	@RequestMapping( value="login",method = [RequestMethod.GET ])
	public String login(
			@NotBlank @DefaultValue( 'default' ) String alias,
			@NotBlank @DefaultValue( 'auto' ) String redirectUri,
			@NotBlank String consumerKey,
			@NotBlank String consumerSecret,
			@ApiIgnore HttpServletRequest request ) {

		Twitter twitter = new TwitterFactory().getInstance()
		twitter.setOAuthConsumer( consumerKey, consumerSecret )
		RequestToken requestToken

		try {
			if ( 'auto'.equals( redirectUri ) )
				redirectUri = "${request.constructUrl()}api/twitter/login/callback/${alias}"
			requestToken = twitter.getOAuthRequestToken( redirectUri )
			"twitter.${alias.trim()}".saveTOROProperty( "${consumerKey.trim()}||${consumerSecret.trim()}||${requestToken.getToken().trim()}||${requestToken.getTokenSecret().trim()}" )
			"redirect:${requestToken.getAuthorizationURL()}"
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping( value="login/callback/{alias}",method = [RequestMethod.GET ])
	public String callback( @NotBlank @PathVariable String alias, 
		@NotBlank String oauth_verifier ) {
		
		def token = "twitter.${alias.trim()}".getTOROProperty()?.split('||')
		Twitter twitter = new TwitterFactory().getInstance()
		twitter.setOAuthConsumer( token[0], token[1] )

		RequestToken requestToken = new RequestToken(token[2], token[3])
		AccessToken accessToken = twitter.getOAuthAccessToken(requestToken,oauth_verifier)
		"twitter.${alias.trim()}".saveTOROProperty( "${token[0].trim()}||${token[1].trim()}||${accessToken.getToken().trim()}||${accessToken.getTokenSecret().trim()}" )
		'html:Token saved'
	}
}