package io.toro.integrate.twitter

import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile

import com.toro.esb.core.api.APIResponse


@RestController
@RequestMapping( value = 'twitter', 
	produces = [ 'application/json', 'application/xml' ] )
class TwitterAdapter {

	/**
	 * Tweet Update in Twitter
	 * @param alias the name of the credentials in toro registry
	 * @param tweet the status that will be posted on twitter
	 * @return returns information about posted status
	 */
	@RequestMapping( value = '/statuses/update', method = [RequestMethod.POST])
	APIResponse tweet(
			@RequestParam String alias,
			@RequestParam String tweet) {
		def response = alias.twitterUpdateStatus( tweet )
		new APIResponse('OK' , response )
	}

	/**
	 * Tweet Update with Media in Twitter
	 * @param alias the name of the credentials in toro registry
	 * @param tweet the status that will be posted on twitter
	 * @param file is the file to be posted together with the tweet
	 * @return returns information about uploaded media and status
	 */
	@RequestMapping( value = '/statuses/update_with_media', method = [RequestMethod.POST])
	APIResponse tweetWithMedia(
			@RequestParam String alias,
			@RequestParam String tweet,
			@RequestParam MultipartFile file) {
		File newfile = new File(file.getOriginalFilename())
		file.transferTo(newfile)
		def response = alias.twitterUpdateStatusWithMedia( tweet, newfile )
		new APIResponse('OK' , response )
	}

	/**
	 * Gets the user Home Timeline
	 * @param alias the name of the credentials in toro registry
	 * @return returns statuses on home timeline
	 */
	@RequestMapping(  value = '/statuses/home_timeline', method = [RequestMethod.GET])
	APIResponse getHomeTimeLine( @RequestParam String alias ) {
		def response = alias.twitterGetHomeTimeline()
		new APIResponse('OK' , response )
	}

	/**
	 * Retweets a Status
	 * @param alias the name of the credentials in toro registry
	 * @param statusID is the identifying number of a status
	 * @return returns information about retweeted status
	 */
	@RequestMapping( value = 'statuses/retweet/{statusID}', method = [RequestMethod.POST])
	APIResponse retweet(
			@RequestParam String alias,
			@PathVariable long statusID) {
		def response = alias.twitterRetweetStatus( statusID )
		new APIResponse('OK' , response )
	}

	/**
	 * Get user timeline
	 * @param alias the name of the credentials in toro registry
	 * @return returns statuses on user timeline
	 */
	@RequestMapping( value = '/statuses', method = [RequestMethod.GET])
	APIResponse getUserTimeline( @RequestParam String alias ) {
		def response = alias.twitterGetUserTimeline()
		new APIResponse('OK', response)
	}
}
