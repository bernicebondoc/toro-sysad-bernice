<%@ page import="com.toro.esb.core.esbpackage.model.ESBPackage" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%
    ESBPackage esbPackage = (ESBPackage)getServletConfig().getServletContext().getAttribute( "ESB_PACKAGE" );
    String packageName = esbPackage.getName();
%>

<head>
    <meta charset="utf-8">
    <title>Twitter Adapter</title>
    <meta name="author" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="/assets/css/uikit/uikit.css">
    <link rel="stylesheet" href="/assets/css/uikit/uikit-components.css">
    <link rel="stylesheet" href="/assets/css/uikit/uikit-docs.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">


    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700">
    <link rel="stylesheet" href="/assets/css/bootstrap/datetimepicker.css">
    <link rel="stylesheet" href="/assets/css/layout-esb.css">
    <link rel="stylesheet" href="/assets/css/layout-error_404.css">
    <!-- jQuery and jQueryUI -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <!-- globalize for graphs -->
    <script src="//ajax.aspnetcdn.com/ajax/globalize/0.1.1/globalize.min.js"></script>
    <script src="/assets/js/vendor/jquery.flot.js"></script>
    <script src="/assets/js/vendor/uikit-flot.js"></script>
    <script src="/assets/js/plugins/moment.js"></script>
    <!-- Bootstrap Plugins -->
    <script src="/assets/js/bootstrap/bootstrap.js"></script>
    <script src="/assets/js/bootstrap/datetimepicker.min.js"></script>
    <!-- UIKit Plugins -->
    <script src="/assets/js/plugins/prettify.js"></script>
    <script src="/assets/js/plugins/plugin.js"></script>
    <script src="/assets/js/plugins/tableSorter.js"></script>
    <script src="/assets/js/plugins/pagination.js"></script>
    <script src="/assets/js/plugins/sticky-navbar.js"></script>
    <script src="/assets/js/plugins/jquery.tooltipster.min.js"></script>
    <script src="/assets/js/plugins/hoverChart.js"></script>
    <script src="/assets/js/docs.js"></script>
    <script src="/assets/esb/js/esb.js"></script>
</head>
<body style="position: relative;">
<div class="bs-docs-header" id="content" <%--style="background-image: linear-gradient(to bottom, #036b92 0%, #0080b0 100%);"--%>>
    <div class="container" id="top">
        <h1 style="width: 100%">Twitter Adapter</h1>

        <p>Read below on how to write beautiful code that seamlessly integrates the with Twitter.</p>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-9" role="main">
            <div class="bs-docs-section">
                <h1 id="overview" class="page-header">
                    Overview
                </h1>

                <p>
                    The Twitter adapter is a small <a href="http://groovy.codehaus.org/">Groovy</a> script
                    that allows you to tweet status updates to your
                    <a href="http://www.twitter.com">Twitter</a> account from
                    your own integrations in the TORO Integrate. Once configured, it's literally a 'one-liner', as shown below.
                    <br/>
                    <br/>
                <pre class="prettyprint lang-groovy linenums">'myTwitterHandle'.tweet('Hello world!')</pre>
                </p>
            </div>
            <div class="bs-docs-section">
                <h1 id="authenticating" class="page-header">Authenticating</h1>

                <h3 id="getting-oauth-keys">Getting OAuth Keys</h3>

                <p>
                    In order to be able to securely tweet against your account, Twitter requires you to register the TORO Integrate adapter
                    with your twitter account. This is done using an industry-standard protocol called <a href="http://oauth.net/">OAuth</a>.
                    Thankfully, this procedure only needs to be executed once.
                </p>

                <h4>Step 1. Register the Twitter  Adapter App</h4>

                <p>
                    First and foremost, if you're not registered or signed in with Twitter, please do so now. Next, visit the <a href="https://apps.twitter.com/">Twitter Apps</a> site. and then click on the <strong>Create New App</strong> button.
                    Twitter will then present you with a form to fill out. Fill out the form, leaving the <strong>Callback URL</strong> field blank.
                </p>

                <h4>Step 2. Change App Permissions</h4>

                <p>
                    Once Step 1 has been completed, Twitter will show you a page with the details of the new app. Click on the
                    <stong>modify app permissions</stong>
                    link and select <strong>Read and Write</strong>, then click <strong>Update Settings</strong>.
                </p>

                <h4>Step 3. Get API Keys</h4>

                <p>
                    The next step is to get the API keys from the Twitter website. Click on the <strong>details</strong>
                    tab in the Twitter apps website, and then click on the <strong>manage keys and access tokens</strong> link, near
                    <strong>Consumer Key (API Key)</strong>. Next, click on the <strong>Create my access token</strong> button at the bottom
                    of the page.
                </p>

                <h4>Step 4. Save details to TORO Integrate</h4>

                <p>
                    The last step is to invoke an service called <a target="_blank"
                                                                        href="/invoker/get/<%=packageName%>/groovy:TwitterRegister/public%20java.lang.String%20TwitterRegister.saveTokenAndSecret%28java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String%29">saveTokenAndSecret</a>.
                    This service will do the last necessary steps for authenticating with Twitter. It will also save the necessary credentials in the TORO Integrate
                    package itself.
                    Enter the <strong>consumerKey (API Key)</strong>, <strong>consumerSecret (API Secret)</strong>, <strong>accessToken</strong> and <strong>accessTokenSecret</strong>
                    from the twitter website.
                    The <strong>alias</strong> field should be something easy to remember and related to the twitter account you're authenticating against, as
                    it's
                    used to tell the TORO Integrate which twitter account
                    to use at runtime. For example, if you use the alias <code>testAccount</code>, then you will tweet against the account with the following
                    code:
                <pre class="prettyprint lang-groovy linenums">'testAccount'.tweet('Hello world!')</pre>
                </p>
            </div>
            <div class="bs-docs-section">
                <h1 id="usage" class="page-header">
                    Usage
                </h1>
                The Twitter adapter comes with two small, easy to use methods. The only difference between the two is
                that one is ansychronous, and the other is not. The asynchronous method <code>asyncTweet</code> publishes the alias
                and tweet to a JMS queue called <code>com.toro.integrate.twitter.Tweet</code>. This adapter comes with a built-in endpoint that
                listens for messages on this queue, and posts them to twitter accordingly. The other synchronous method posts to twitter directly.
                <pre class="prettyprint lang-groovy linenums">'myTwitterHandle'.tweet('Hello world!') // synchronous - returns the post ID
                    'myTwitterHandle'.asyncTweet('Hello world async!') // asynchronous - returns nothing, but does return instantly.</pre>
                You can test the services here:
                <ul>
                    <li><a target="_blank"
                           href="/invoker/get/<%=packageName%>/groovy:Twitter/public static java.lang.Object Twitter.tweet(java.lang.String,java.lang.String)">tweet</a>
                    </li>
                    <li><a target="_blank"
                           href="/invoker/get/<%=packageName%>/groovy:Twitter/public static java.lang.Object Twitter.asyncTweet(java.lang.String,java.lang.String)">asyncTweet</a>
                    </li>
                </ul>

            </div>

        </div>
        <div class="col-md-3">
            <div class="bs-docs-sidebar hidden-print hidden-xs hidden-sm affix-top" role="complementary">
                <ul class="nav bs-docs-sidenav">
                    <li class="active"><a href="#overview">Overview</a></li>
                    <li>
                        <a href="#authenticating">Authenticating</a>
                    </li>
                    <li>
                        <a href="#usage">Usage</a>
                    </li>
                </ul>
                <a class="back-to-top" href="#top">
                    Back to top
                </a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    window.prettyPrint && prettyPrint();
</script>

</body>
</html>