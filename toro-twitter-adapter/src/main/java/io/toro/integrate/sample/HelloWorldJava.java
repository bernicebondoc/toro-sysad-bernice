package io.toro.integrate.sample;

public class HelloWorldJava {

    public static String sayHelloJava( String name ) {
        return String.format( "Hello %s, from java code", name );
    }
}
